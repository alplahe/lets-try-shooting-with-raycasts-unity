﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShoot : MonoBehaviour
{
	public Rigidbody m_projectile;
	public Transform m_bulletSpawn;
	public float m_projectileForce = 250f;

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{
		DoShot();
	}

	private void DoShot()
	{
		if (Input.GetButtonDown("Fire2"))
		{
			Launch();
		}
	}
	
	public void Launch()
	{
		Rigidbody clonedBullet = Instantiate(
			m_projectile, 
			m_bulletSpawn.position, 
			transform.rotation * Quaternion.Euler(90,0,0)) as Rigidbody;
		
		clonedBullet.AddForce(m_bulletSpawn.transform.forward * m_projectileForce);
	}
}
